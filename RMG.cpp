#include <iostream>
#include <math.h>
#include <time.h>

#define ARMA_DONT_USE_CXX11
#include <armadillo>


void scale(const int Nfract, const int Mfract, arma::mat &field)
{
  double maxVal = field(0,0);
  double minVal = field(0,0);

  for( int i = 0; i < Nfract; i++ )
    for( int j = 0; j < Mfract; j++ )
      {
        if(maxVal < field(i,j)) maxVal = field(i,j);
        if(minVal > field(i,j)) minVal = field(i,j);
      }

  double s = 1./(maxVal - minVal);

  for( int i = 0; i < Nfract; i++ )
    for( int j = 0; j < Mfract; j++ )
      field(i,j) = s*(field(i,j) - minVal);
}

void diamondStep(const int i, const int j, const int step, arma::mat &field, 
                 const double rnd)
{
  field((2*i + 1)*step,(2*j + 1)*step) = 
    0.25*(field(2*i*step,2*j*step) 
          + field((2*i + 2)*step,2*j*step)
          + field(2*i*step,(2*j + 2)*step)
          + field((2*i + 2)*step,(2*j + 2)*step))
    + rnd;
}

void squareStepEven(const int i, const int j, const int step, arma::mat &field, 
                    const double rnd)
{
  field((2*i + 2)*step,(2*j + 1)*step) = 
    0.25*(field((2*i + 2)*step,2*j*step) 
          + field((2*i + 1)*step,(2*j + 1)*step)
          + field((2*i + 3)*step,(2*j + 1)*step)
          + field((2*i + 2)*step,(2*j + 2)*step)) 
    + rnd;
}

void squareStepOdd(const int i, const int j, const int step, arma::mat &field, 
                   const double rnd)
{
  field((2*i + 1)*step,(2*j + 2)*step) = 
    0.25*(field(2*i*step,(2*j + 2)*step) 
          + field((2*i + 1)*step,(2*j + 1)*step)
          + field((2*i + 1)*step,(2*j + 3)*step)
          + field((2*i + 2)*step,(2*j + 2)*step))
    + rnd;
}

void squareStepBCTop(const int i, const int step, arma::mat &field, 
                     const double rnd)
{
  field((2*i + 1)*step,0) = 
    0.333*(field(2*i*step,0)
           + field((2*i + 2)*step,0)
           + field((2*i + 1)*step,step))
    + rnd;
}

void squareStepBCBottom(const int i, const int Mfract, const int step, 
                        arma::mat &field, const double rnd)
{
  field((2*i + 1)*step,Mfract - 1) = 
    0.333*(field(2*i*step,Mfract - 1)
           + field((2*i + 2)*step,Mfract - 1)
           + field((2*i + 1)*step,Mfract - 1 - step))
    + rnd;
}

void squareStepBCLeft(const int j, const int step, arma::mat &field, 
                      const double rnd)
{
  field(0,(2*j + 1)*step) = 
    0.333*(field(0,2*j*step)
           + field(0,(2*j + 2)*step)
           + field(step,(2*j + 1)*step))
    + rnd;
}

void squareStepBCRight(const int j, const int Nfract, const int step, 
                       arma::mat &field, const double rnd)
{
  field(Nfract - 1,(2*j + 1)*step) =
    0.333*(field(Nfract - 1,2*j*step)
           + field(Nfract - 1,(2*j + 2)*step)
           + field(Nfract - 1 -step,(2*j + 1)*step))
    + rnd;
}

double myRand(double lowerBound, double upperBound)
{
  if(lowerBound >= upperBound)
    {
      std::cout << "\n Error in myRand: \n"
                << " lowerBound >= upperBound \n";
     
      exit(EXIT_FAILURE);
    }
  
  double range = upperBound - lowerBound;

  return rand()%(static_cast<int>(range*1000))*0.001 + lowerBound;
}

double getRandom(const double roughness, const int k)
{
  return myRand(-1,1)*pow(roughness,k);
}


int main()
{
  srand(time(NULL));

  // Hexa mesh dimensions
  int N = 50;
  int M = 50;

  enum class region { O, T, S, F, P, J, D, M };

  arma::Mat<int> regions(N, M);
  regions.fill(static_cast<int>(region::O));

  int power = log2(fmax(N,M)) + 1;
  std::cout << "power: " << power << "\n";
  int Nfract = pow(2,power) + 1;
  int Mfract = pow(2,power) + 1;

  double Hcoeff = 0.8;
  double roughness = pow(2.0, -Hcoeff);

  arma::mat terrain(Nfract, Mfract);
  arma::mat temp(Nfract, Mfract);
  arma::mat precip(Nfract, Mfract);
  arma::mat drainage(Nfract, Mfract);

  terrain.fill(0.0);
  temp.fill(0.0);
  precip.fill(0.0);
  drainage.fill(0.0);

  // Fractal generation
  // Corner seeds
  terrain(1,2) = myRand(-1,1);
  terrain(Nfract-1,0) = myRand(-1,1);
  terrain(0,Mfract-1) = myRand(-1,1);
  terrain(Nfract-1,Mfract-1) = myRand(-1,1);

  precip(0,0) = myRand(-1,1);
  precip(Nfract-1,0) = myRand(-1,1);
  precip(0,Mfract-1) = myRand(-1,1);
  precip(Nfract-1,Mfract-1) = myRand(-1,1);

  drainage(0,0) = myRand(-1,1);
  drainage(Nfract-1,0) = myRand(-1,1);
  drainage(0,Mfract-1) = myRand(-1,1);
  drainage(Nfract-1,Mfract-1) = myRand(-1,1);

  for( int k = 0; k < power; k++ )
    {
      int step = pow(2, power - 1 - k);
      
      // "Diamond" step
      std::cout << "Diamond step ...";
      for( int i = 0; i < pow(2,k); i++ )
        {
          for( int j = 0; j < pow(2,k); j++ )
            {
              diamondStep(i, j, step, terrain, getRandom(roughness, k));
              diamondStep(i, j, step, precip, getRandom(roughness, k));
              diamondStep(i, j, step, drainage, getRandom(roughness, k));
            }
        }
      std::cout << "done\n";
      
      // "Square" step - boundaries
      std::cout << "Square step - boundaries ...";
      for( int i = 0; i < pow(2,k); i++ )
        {
          squareStepBCTop(i, step, terrain, getRandom(roughness, k));
          squareStepBCTop(i, step, precip, getRandom(roughness, k));
          squareStepBCTop(i, step, drainage, getRandom(roughness, k));
          
          squareStepBCBottom(i, Mfract, step, terrain, getRandom(roughness, k));
          squareStepBCBottom(i, Mfract, step, precip, getRandom(roughness, k));
          squareStepBCBottom(i, Mfract, step, drainage, getRandom(roughness, k));
        }
      
      for( int j = 0; j < pow(2,k); j++ )
        {
          squareStepBCLeft(j, step, terrain, getRandom(roughness, k));
          squareStepBCLeft(j, step, precip, getRandom(roughness, k));
          squareStepBCLeft(j, step, drainage, getRandom(roughness, k));
          
          squareStepBCRight(j, Mfract, step, terrain, getRandom(roughness, k));
          squareStepBCRight(j, Mfract, step, precip, getRandom(roughness, k));
          squareStepBCRight(j, Mfract, step, drainage, getRandom(roughness, k));
        }
      std::cout << "done\n";
      
      // "Square" step - internal field
      std::cout << "Square step - internal field ...";
      for( int i = 0; i < pow(2,k); i++ )
        {
          for( int j = 0; j < pow(2,k); j++ )
            {
              if( (2*i + 3)*step < Nfract)
                {
                  squareStepEven(i, j, step, terrain, getRandom(roughness, k));
                  squareStepEven(i, j, step, precip, getRandom(roughness, k));
                  squareStepEven(i, j, step, drainage, getRandom(roughness, k));
                }
              
              if( (2*j + 3)*step < Mfract)
                {
                  squareStepOdd(i, j, step, terrain, getRandom(roughness, k));
                  squareStepOdd(i, j, step, precip, getRandom(roughness, k));
                  squareStepOdd(i, j, step, drainage, getRandom(roughness, k));
                }
            }
        }
      std::cout << "done\n";
    }
  
  scale(Nfract, Mfract, terrain);
  scale(Nfract, Mfract, precip);
  scale(Nfract, Mfract, drainage);
  
  for( int i = 0; i < Nfract; i++ )
    {
      for( int j = 0; j < Mfract; j++ )
        {
          temp(i,j) = 1.0*i/Nfract;
        }
    }
  
  /*
  for( int i = 0; i < Nfract; i++ )
    {
      for( int j = 0; j < Mfract; j++ )
        {
          //          std::cout << drainage(i,j)/precip(i,j) << " ";
          std::cout << terrain(i,j) << " ";
        }
      std::cout << "\n";
    }
  */

  for( int i = 0; i < N; i++ )
    {
      for( int j = 0; j < M; j++ )
        {
          if( (i+j)%2 == 0) 
            {
              // +/- 20%
              double s = myRand(0.8,1.2);
              
              if( terrain(i,j) < 0.25*s ) regions(i,j) = 
                                            static_cast<int>(region::O);
              else if( terrain (i,j) > 0.75*s ) regions(i,j) = 
                                                  static_cast<int>(region::M);
              else
                {
                  double dryiness = drainage(i,j)/precip(i,j);

                  if( temp(i,j) > 0.667*s ) 
                    {
                      if( dryiness > 1.2*s ) regions(i,j) = 
                                               static_cast<int>(region::D);
                      else if( dryiness < 0.8*s ) regions(i,j) = 
                                                    static_cast<int>(region::J);
                      else regions(i,j) = static_cast<int>(region::P);
                    }
                  else if ( temp(i,j) < 0.333*s ) regions(i,j) = 
                                                    static_cast<int>(region::T);
                  else 
                    {
                      if( dryiness > 1.5*s ) regions(i,j) = 
                                               static_cast<int>(region::P);
                      else if( dryiness < 0.5*s ) regions(i,j) = 
                                                    static_cast<int>(region::S);
                      else regions(i,j) = static_cast<int>(region::F);
                    }
                }
              
              std::cout << regions(i,j); 
            }
          else std::cout << " ";
        }
      
      std::cout << "\n";
    }
  
  return 0;
}
